/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.4.17-MariaDB : Database - db_project_kel5
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_project_kel5` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `db_project_kel5`;

/*Table structure for table `barang` */

DROP TABLE IF EXISTS `barang`;

CREATE TABLE `barang` (
  `kode_barang` char(11) NOT NULL,
  `nama_barang` varchar(50) DEFAULT NULL,
  `jumlah` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `barang` */

insert  into `barang`(`kode_barang`,`nama_barang`,`jumlah`) values 
('001','Proyektor','4 Unit'),
('002','Meja','20 Unit'),
('003','Monitor','20 Unit');

/*Table structure for table `pengajuan` */

DROP TABLE IF EXISTS `pengajuan`;

CREATE TABLE `pengajuan` (
  `kode_pengajuan` char(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `npm_peminjam` varchar(50) DEFAULT NULL,
  `nama_peminjam` varchar(50) DEFAULT NULL,
  `prodi` varchar(50) DEFAULT NULL,
  `no_handphone` char(15) DEFAULT NULL,
  PRIMARY KEY (`kode_pengajuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `pengajuan` */

insert  into `pengajuan`(`kode_pengajuan`,`tanggal`,`npm_peminjam`,`nama_peminjam`,`prodi`,`no_handphone`) values 
('P001','2023-06-17','20311427','Putri kumala Sari','SI','08272361512'),
('P002','2023-06-19','20311429','Dhea Ananda','SI','08625365162'),
('P003','2023-06-21','20311426','Elvika Alya Junita','SI','08726152623'),
('P004','2023-06-17','20311080','Eskiyaturrofikoh','SI','08726352634');

/*Table structure for table `pengajuan_detail` */

DROP TABLE IF EXISTS `pengajuan_detail`;

CREATE TABLE `pengajuan_detail` (
  `kode_pengajuan` char(11) DEFAULT NULL,
  `kode_barang` char(11) DEFAULT NULL,
  `jumlah` varchar(50) DEFAULT NULL,
  KEY `kode_pengajuan` (`kode_pengajuan`),
  KEY `kode_barang` (`kode_barang`),
  CONSTRAINT `pengajuan_detail_ibfk_1` FOREIGN KEY (`kode_pengajuan`) REFERENCES `pengajuan` (`kode_pengajuan`),
  CONSTRAINT `pengajuan_detail_ibfk_2` FOREIGN KEY (`kode_barang`) REFERENCES `barang` (`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `pengajuan_detail` */

insert  into `pengajuan_detail`(`kode_pengajuan`,`kode_barang`,`jumlah`) values 
('P001','001','2 Unit'),
('P002','002','3 Unit'),
('P003','003','5 Unit');

/*Table structure for table `pengembalian` */

DROP TABLE IF EXISTS `pengembalian`;

CREATE TABLE `pengembalian` (
  `kode_pengembalian` char(11) NOT NULL,
  `kode_pengajuan` char(11) DEFAULT NULL,
  `tanggal_kembali` date DEFAULT NULL,
  PRIMARY KEY (`kode_pengembalian`),
  KEY `kode_pengajuan` (`kode_pengajuan`),
  CONSTRAINT `pengembalian_ibfk_1` FOREIGN KEY (`kode_pengajuan`) REFERENCES `pengajuan` (`kode_pengajuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `pengembalian` */

insert  into `pengembalian`(`kode_pengembalian`,`kode_pengajuan`,`tanggal_kembali`) values 
('K001','P002','2023-06-19'),
('K002','P001','2023-06-20'),
('K003','P002','2023-06-22');

/*Table structure for table `pengembalian_detail` */

DROP TABLE IF EXISTS `pengembalian_detail`;

CREATE TABLE `pengembalian_detail` (
  `kode_pengembalian` char(11) DEFAULT NULL,
  `kode_barang` char(11) DEFAULT NULL,
  `jumlah` varchar(50) DEFAULT NULL,
  KEY `kode_pengembalian` (`kode_pengembalian`),
  KEY `kode_barang` (`kode_barang`),
  CONSTRAINT `pengembalian_detail_ibfk_1` FOREIGN KEY (`kode_pengembalian`) REFERENCES `pengembalian` (`kode_pengembalian`),
  CONSTRAINT `pengembalian_detail_ibfk_2` FOREIGN KEY (`kode_barang`) REFERENCES `barang` (`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `pengembalian_detail` */

insert  into `pengembalian_detail`(`kode_pengembalian`,`kode_barang`,`jumlah`) values 
('K001','001','2 Unit'),
('K002','002','4 Unit'),
('K003','003','4 Unit');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `pass` varchar(225) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `user` */

insert  into `user`(`id_user`,`name`,`username`,`pass`,`role`) values 
(1,'putri','putri','28b662d883b6d76fd96e4ddc5e9ba780','admin'),
(2,'Dea','user','96991368fec63c8a1bfc48a70010f84a','Admin');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
